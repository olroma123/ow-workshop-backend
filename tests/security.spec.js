const chai = require('chai');
const expect = chai.expect;
const axios = require('axios').default;
let apiEnv = require('../config/dev.env');
apiEnv.host = `${apiEnv.backend.host}:${apiEnv.backend.port}`;
const atob = require('atob');
const uuid = require('uuid/v4');

const database = require('../app/lib/database');
const UserModel = require('../app/models/user');

describe('Регистрация', () => {
  describe('Проверка параметров', () => {
    it('Не переданы все параметры с body. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
      return axios.post(`http://${apiEnv.host}/security/register`, {})
        .catch(err => {
          expect(err.response.status).to.equal(400);
          expect(err.response.data).to.have.property('message');
          expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
        });
    });

    it('Не передан e-mail. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
      return axios.post(`http://${apiEnv.host}/security/register`, { password: 'test'}).then()
        .catch(err => {
          expect(err.response.status).to.equal(400);
          expect(err.response.data).to.have.property('message');
          expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
        });
    });

    it('Не передан password. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
      return axios.post(`http://${apiEnv.host}/security/register`, {email: 'test@test.ru'}).then()
        .catch(err => {
          expect(err.response.status).to.equal(400);
          expect(err.response.data).to.have.property('message');
          expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
        });
    });

    describe('E-mail', () => {
      it('В качестве e-mail было передано числовой тип. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
        return axios.post(`http://${apiEnv.host}/security/register`, {
          email: 1,
          password: 'test'
        }).then()
          .catch(err => {
            expect(err.response.status).to.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
          });
      });

      it('Неправильный формат e-mail. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
        return axios.post(`http://${apiEnv.host}/security/register`, {
          email: 'test.ru',
          password: 'test'
        }).then()
          .catch(err => {
            expect(err.response.status).to.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
          });
      });
    });

    describe('Пароль', () => {
      it('В качестве пароля было передано числовой тип. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
        return axios.post(`http://${apiEnv.host}/security/register`, {
          email: 'test@test.ru',
          password: 1
        }).then()
          .catch(err => {
            expect(err.response.status).to.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
          });
      });
    });
  });

  describe('Общая функциональность', () => {
    before(() => {
      return new Promise((resolve, reject) => {
        // Проверям тестового пользователя
        // Если существует - удаляем
        database.connect()
          .then(() => UserModel.isExistsByEmail({email: 'test1@test.ru'}))
          .then(isExists => {
            if (isExists) {
              return UserModel.deleteByEmail({email: 'test1@test.ru'})
            }

            resolve();
          }).then(() => resolve());
      })
    });

    it('Успешная регистрация. Должен вернуть статус 200 и объект с валидным jwt токеном', () => {
      return axios.post(`http://${apiEnv.host}/security/register`, {
        email: 'test1@test.ru',
        password: 'test'
      }).then(response => {
        expect(response.status).to.be.equal(200);
        expect(response.data).to.have.property('message');
        expect(response.data.message).to.have.property('code').that.equal('API.OK');

        const userData = JSON.parse(decodeURIComponent(escape(atob(response.data.payload.token.split('.')[1]))));
        expect(userData.role).to.have.all.keys(['title', 'name']);
        expect(userData.role.title).to.equal('Пользователь');
        expect(userData.role.name).to.equal('user');
      });
    });

    it('После регистрации в базе должен быть наш тестовый пользователь', () => {
      return database.collection('users').findOne({email: 'test1@test.ru'})
        .then(user => {
          return expect(user).to.not.be.null;
        })
    });

    it('Регистрация уже существующего e-mail. Должен вернуть статус 409 и код ошибки должен быть User.AlreadyExists', () => {
      return axios.post(`http://${apiEnv.host}/security/register`, {
        email: 'test1@test.ru',
        password: 'test'
      }).catch(err => {
        expect(err.response.status).to.be.equal(409);
        expect(err.response.data).to.have.property('message');
        expect(err.response.data.message).to.have.property('code').that.equal('User.AlreadyExists');
      })
    });

    it('После повторной регистрации в базе данных должен быль лишь один пользователь с таким E-mail', () => {
      return database.collection('users').find({email: 'test1@test.ru'}).toArray()
        .then(data => {
          expect(data).to.have.length(1);
        })
    });
  });
});

describe('Авторизация', () => {
  describe('Проверка параметров', () => {
    it('Не переданы все параметры с body. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
      return axios.post(`http://${apiEnv.host}/security/auth`, {}).then()
        .catch(err => {
          expect(err.response.status).to.equal(400);
          expect(err.response.data).to.have.property('message');
          expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
        });
    });

    it('Не передан e-mail. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
      return axios.post(`http://${apiEnv.host}/security/auth`, {
        password: 'test'
      }).then()
        .catch(err => {
          expect(err.response.status).to.equal(400);
          expect(err.response.data).to.have.property('message');
          expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
        });
    });

    it('Не передан password. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
      return axios.post(`http://${apiEnv.host}/security/auth`, {
        email: 'test@test.ru'
      }).then()
        .catch(err => {
          expect(err.response.status).to.equal(400);
          expect(err.response.data).to.have.property('message');
          expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
        });
    });

    describe('E-mail', () => {
      it('В качестве e-mail было передано числовой тип. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
        return axios.post(`http://${apiEnv.host}/security/auth`, {
          email: 1,
          password: 'test'
        }).then()
          .catch(err => {
            expect(err.response.status).to.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
          });
      });

      it('Неправильный формат e-mail. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
        return axios.post(`http://${apiEnv.host}/security/auth`, {
          email: 'test.ru',
          password: 'test'
        }).then()
          .catch(err => {
            expect(err.response.status).to.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
          });
      });
    });

    describe('Пароль', () => {
      it('В качестве пароля было передано числовой тип. Должен вернуть статус 400 и код ошибки должен быть API.WrongParams', () => {
        return axios.post(`http://${apiEnv.host}/security/auth`, {
          email: 'test@test.ru',
          password: 1
        }).then()
          .catch(err => {
            expect(err.response.status).to.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
          });
      });
    });
  });

  describe('Функциональность', () => {
    it('Несуществующий аккаунт. Должен вернуть статус 403 и код ошибки должен быть Security.WrongLoginOrPassword', () => {
      return axios.post(`http://${apiEnv.host}/security/auth`, {
        email: 'uncreated@uncreated.ru',
        password: 'uncreated'
      }).then()
        .catch(err => {
          expect(err.response.status).to.equal(403);
          expect(err.response.data).to.have.property('message');
          expect(err.response.data.message).to.have.property('code').that.equal('Security.WrongLoginOrPassword');
        });
    });

    it('Успешная авторизация через тестовый аккаунт. Должен вернуть статус 200 и объект с пол. данными внутри токена', () => {
      return axios.post(`http://${apiEnv.host}/security/auth`, {
        email: 'test1@test.ru',
        password: 'test'
      }).then(response => {
        expect(response.status).to.equal(200);
        expect(response.data).to.have.property('message');
        expect(response.data.message).to.have.property('code').that.equal('API.OK');

        expect(response.data.payload).to.have.property('token');
        const userData = JSON.parse(decodeURIComponent(escape(atob(response.data.payload.token.split('.')[1]))));

        expect(userData).to.have.all.keys([
          'email', 'id', 'role', 'refresh_token', 'iat', 'exp'
        ]);

        expect(userData.email).to.equal('test1@test.ru');
        expect(userData.role).to.have.all.keys(['title', 'name']);
        expect(userData.role.title).to.equal('Пользователь');
        expect(userData.role.name).to.equal('user');

        // Проверка refresh_token'а
        return database.collection('users').findOne({email: 'test1@test.ru'})
          .then(user => {
            const foundToken = !!user.refresh_tokens.find(token => token.token === userData.refresh_token);
            expect(foundToken).to.equal(true);
          });
      });
    });
  });
});

describe('Обновление refresh токена', () => {
  it('Передан токен, которого нет в бд. Должен вернуть 403 и код Security.AccessDenied', async () => {
    const user = await UserModel.getByEmail({email: 'test1@test.ru'});
    const result = await UserModel.updateRefreshToken(user, 'test', uuid());
    expect(result).to.have.property('error');
    expect(result.error).to.be.equal(true);
    expect(result.code).to.be.equal('Security.AccessDenied')
  });

  it('Передан валидный токен. После обновления в бд должен удалиться старый токен и добавиться новый', async () => {
    const user = await UserModel.getByEmail({email: 'test1@test.ru'});
    const rToken = user.refresh_tokens[0].token;
    const newToken = uuid();
    await UserModel.updateRefreshToken(user, rToken, newToken);

    const userAfterRefreshUpdate = await UserModel.getByEmail({email: 'test1@test.ru'});
    const foundOldToken = !!userAfterRefreshUpdate.refresh_tokens.find(token => token.token === rToken);
    const foundNewToken = !!userAfterRefreshUpdate.refresh_tokens.find(token => token.token === newToken);

    expect(foundOldToken).to.equal(false);
    expect(foundNewToken).to.equal(true);
  });
});
