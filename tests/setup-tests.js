const chai = require('chai');
const expect = chai.expect;

const atob = require('atob');
const database = require('../app/lib/database');
const UserModel = require('../app/models/user');
const RoleModel = require('../app/models/role');

const testUsers = {
  admin: {
    login: 'test-admin@test.ru',
    password: 'test'
  },

  user: {
    login: 'test-user@test.ru',
    password: 'test'
  }
};

describe('Подготовка тестовых данных', () => {
  before(() => {
    return database.connect();
  });

  describe('Создание пользователей', () => {
    it('Администратор', () => {
      return UserModel.isExistsByEmail({email: testUsers.admin.login })
        .then(exists => {
          const waiting = [];
          if (!exists) {
            waiting.push(UserModel.add({email: testUsers.admin.login, password: testUsers.admin.password}));
          } else {
            waiting.push(UserModel.getByEmail({email: testUsers.admin.login}));
          }

          return Promise.all(waiting);
        })
        .then(([token]) => {
          const user = typeof token === 'string' ?
            JSON.parse(decodeURIComponent(escape(atob(token.split('.')[1])))) :
            token;

          return UserModel.changeRole(user._id || user.id, RoleModel.getUserRole());
        })
        .then(() => {
          expect(true).to.be.equal(true);
        });
    });

    it('Тестовый пользователь', () => {
      return UserModel.isExistsByEmail({ email: testUsers.user.login })
        .then(exists => {
          const waiting = [];
          if (!exists) {
            waiting.push(UserModel.add({email: testUsers.user.login, password: testUsers.user.password}));
          } else {
            waiting.push(true);
          }

          return Promise.all(waiting);
        })
        .then(() => {
          expect(true).to.be.equal(true);
        });
    })
  });
});