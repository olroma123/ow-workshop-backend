const chai = require('chai');
const expect = chai.expect;
const axios = require('axios').default;
const atob = require('atob');
const database = require('../../app/lib/database');
let apiEnv = require('../../config/dev.env');
apiEnv.host = `${apiEnv.backend.host}:${apiEnv.backend.port}`;

describe('/workshop', () => {
  let user1 = null, user2 = null;
  let workshopItemId = null;

  before(() => {
    return new Promise(async (resolve, reject) => {
      const user1ApiData = await axios.post(`http://${apiEnv.host}/security/auth`, {
        email: 'test-admin@test.ru',
        password: 'test'
      });

      const user2ApiData = await axios.post(`http://${apiEnv.host}/security/auth`, {
        email: 'test-user@test.ru',
        password: 'test'
      });

      const user1Data = JSON.parse(decodeURIComponent(escape(atob(user1ApiData.data.payload.token.split('.')[1]))));
      const user2Data = JSON.parse(decodeURIComponent(escape(atob(user2ApiData.data.payload.token.split('.')[1]))));

      user1 = {
        token: user1ApiData.data.payload.token,
        data: user1Data
      };

      user2 = {
        token: user2ApiData.data.payload.token,
        data: user2Data
      };

      resolve();
    });
  });

  describe('Добавление режима (POST /workshop)', () => {
    describe('Безопасность', () => {
      it('Вызов без токена. Статус 401, код ошибки Security.TokenRequired', () => {
        return axios.post(`http://${apiEnv.host}/workshop`, {})
          .catch(err => {
            expect(err.response.status).to.be.equal(401);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('Security.TokenRequired');
          });
      });
    });

    describe('Проверка параметров', () => {
      describe('Обязательные параметры', () => {
        it('Пустой body. Статус 400, код ошибки API.WrongParams', () => {
          return axios.post(`http://${apiEnv.host}/workshop`, {}, {
            headers: { Authorization: `Bearer ${user1.token}` }
          }).catch(err => {
              expect(err.response.status).to.be.equal(400);
              expect(err.response.data).to.have.property('message');
              expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
          });
        });

        it('Не передан code, smallDescription, thumbnails. Статус 400, код ошибки API.WrongParams', () => {
          return axios.post(`http://${apiEnv.host}/workshop`, {
            title: 'Тест режим 1'
          }, {
            headers: { Authorization: `Bearer ${user1.token}` }
          }).catch(err => {
            expect(err.response.status).to.be.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
          });
        });

        it('Не передан smallDescription, thumbnails. Статус 400, код ошибки API.WrongParams', () => {
          return axios.post(`http://${apiEnv.host}/workshop`, {
            title: 'Тест режим 1',
            code: 'TEST1'
          }, {
            headers: { Authorization: `Bearer ${user1.token}` }
          }).catch(err => {
            expect(err.response.status).to.be.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
          });
        });

        it('Не передан thumbnails. Статус 400, код ошибки API.WrongParams', () => {
          return axios.post(`http://${apiEnv.host}/workshop`, {
            title: 'Тест режим 1',
            code: 'TEST1',
            smallDescription: 'Тестовое описание 1'
          }, {
            headers: { Authorization: `Bearer ${user1.token}` }
          }).catch(err => {
            expect(err.response.status).to.be.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
          });
        });
      });

      it('Фейковые URL скринов', () => {
        return axios.post(`http://${apiEnv.host}/workshop`, {
          title: 'Тест режим 1',
          code: 'TEST1',
          smallDescription: 'Тестовое описание 1',
          thumbnails: [
            'http://i.imgur.com/bla.jpg',
            'http://i.imgur.com/bla.jpg',
            'http://i.imgur.com/bla.jpg'
          ]
        }, {
          headers: { Authorization: `Bearer ${user1.token}` }
        }).catch(err => {
          expect(err.response.status).to.be.equal(400);
          expect(err.response.data).to.have.property('message');
          expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
        });
      });
    });

    describe('Проверка метода', () => {
      it('Добавление. Статус 200. В payload должен быть id', () => {
        return axios.post(`http://${apiEnv.host}/workshop`, {
          title: 'Тест режим 1',
          code: 'TEST1',
          smallDescription: 'Тестовое описание 1',
          thumbnails: [
            'https://i.imgur.com/mzxgfNQ.jpg',
            'https://i.imgur.com/mzxgfNQ.jpg',
            'https://i.imgur.com/mzxgfNQ.jpg'
          ]
        }, {
          headers: { Authorization: `Bearer ${user1.token}` }
        }).then(response => {
          expect(response.status).to.be.equal(200);
          expect(response.data).to.have.keys('message', 'payload');
          expect(response.data.message).to.have.property('code').that.equal('API.OK');
          expect(response.data.payload).to.have.property('id').that.not.null;
          workshopItemId = response.data.payload.id;
        });
      });

      it('В бд должен появится наш режим с правильными данными', () => {
        return database.collection('workshop').findOne({_id: database.ObjectId(workshopItemId)})
          .then(item => {
            expect(item).to.not.be.null;
            expect(item.title).to.be.equal('Тест режим 1');
            expect(item.code).to.be.equal('TEST1');
            expect(item.smallDescription).to.be.equal('Тестовое описание 1');
            expect(item.thumbnails).to.be.an('array').and.have.ordered.members([
              'https://i.imgur.com/mzxgfNQ.jpg',
              'https://i.imgur.com/mzxgfNQ.jpg',
              'https://i.imgur.com/mzxgfNQ.jpg'
            ]);
            expect(item.status).to.be.equal('pending');
          });
      });

      it('Попытка добавить режим с таким же кодом. Статус 409, код ошибки Workshop.CodeAlreadyExists', () => {
        return axios.post(`http://${apiEnv.host}/workshop`, {
          title: 'Тест режим 2',
          code: 'TEST1',
          smallDescription: 'Тестовое описание 2',
          thumbnails: [
            'https://i.imgur.com/mzxgfNQ.jpg',
            'https://i.imgur.com/mzxgfNQ.jpg',
            'https://i.imgur.com/mzxgfNQ.jpg'
          ]
        }, {
          headers: { Authorization: `Bearer ${user1.token}` }
        }).catch(err => {
          expect(err.response.status).to.be.equal(409);
          expect(err.response.data).to.have.property('message');
          expect(err.response.data.message).to.have.property('code').that.equal('Workshop.CodeAlreadyExists');
        });
      });
    });
  });

  describe('Добавление полного описания (PATH /workshop/end)', () => {
    describe('Безопасность', () => {
      it('Вызов без токена. Статус 401, код ошибки Security.TokenRequired', () => {
        return axios.patch(`http://${apiEnv.host}/workshop/end`, {})
          .catch(err => {
            expect(err.response.status).to.be.equal(401);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('Security.TokenRequired');
          });
      });

      it('Пользователь не является автором. Статус 403, код ошибки Workshop.NotHaveRightsToEdit', () => {
        return axios.patch(`http://${apiEnv.host}/workshop/end`, {
          id: workshopItemId,
          fullDescription: 'Тестовое полное описание 1'
        }, {headers: { Authorization: `Bearer ${user2.token}` }})
          .catch(err => {
            expect(err.response.status).to.be.equal(403);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('Workshop.NotHaveRightsToEdit');
          });
      });
    });

    describe('Проверка параметров', () => {
      describe('Обязательные параметры', () => {
        it('Пустой body. Статус 400, код ошибки API.WrongParams', () => {
          return axios.patch(`http://${apiEnv.host}/workshop/end`, {},
            {headers: { Authorization: `Bearer ${user1.token}` }})
            .catch(err => {
              expect(err.response.status).to.be.equal(400);
              expect(err.response.data).to.have.property('message');
              expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
            });
        });

        it('Не передан fullDescription. Статус 400, код ошибки API.WrongParams', () => {
          return axios.patch(`http://${apiEnv.host}/workshop/end`, {
            id: workshopItemId
          },{headers: { Authorization: `Bearer ${user1.token}` }})
            .catch(err => {
              expect(err.response.status).to.be.equal(400);
              expect(err.response.data).to.have.property('message');
              expect(err.response.data.message).to.have.property('code').that.equal('API.WrongParams');
            });
        });
      });

      it('Несуществующий ID. Статус 400, код ошибки Workshop.NotFound', () => {
        return axios.patch(`http://${apiEnv.host}/workshop/end`, {
          id: '111111111111111111111111',
          fullDescription: 'Тестовое описание 1'
        },{headers: { Authorization: `Bearer ${user1.token}` }})
          .catch(err => {
            expect(err.response.status).to.be.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('Workshop.NotFound');
          });
      });

      it('Неправильный ID. Статус 400, код ошибки API.NotValidId', () => {
        return axios.patch(`http://${apiEnv.host}/workshop/end`, {
          id: 'not_valid_id',
          fullDescription: 'Тестовое описание 1'
        },{headers: { Authorization: `Bearer ${user1.token}` }})
          .catch(err => {
            expect(err.response.status).to.be.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('API.NotValidId');
          });
      });
    });

    describe('Проверка метода', () => {
      it('Вызов метода. Статус 200', () => {
        return axios.patch(`http://${apiEnv.host}/workshop/end`, {
          id: workshopItemId,
          fullDescription: 'Тестовое полное описание 1'
        },{headers: { Authorization: `Bearer ${user1.token}` }})
          .then(response => {
            expect(response.status).to.be.equal(200);
            expect(response.data).to.have.property('message');
            expect(response.data.message).to.have.property('code').that.equal('API.OK');
          });
      });

      it('Проверка, что в бд статус добавленного режима изменился и присутствует поле fullDescription', () => {
        return database.collection('workshop').findOne({_id: database.ObjectId(workshopItemId)})
          .then(item => {
            expect(item).to.not.be.null;
            expect(item).to.have.property('fullDescription').that.equal('Тестовое полное описание 1');
            expect(item.status).to.be.equal('added');
          })
      });

      it('Попытка снова вызвать метод. Статус 400, код ошибки Workshop.NotPendingStatus', () => {
        return axios.patch(`http://${apiEnv.host}/workshop/end`, {
          id: workshopItemId,
          fullDescription: 'Тестовое полное описание 1'
        },{headers: { Authorization: `Bearer ${user1.token}` }})
          .catch(err => {
            expect(err.response.status).to.be.equal(400);
            expect(err.response.data).to.have.property('message');
            expect(err.response.data.message).to.have.property('code').that.equal('Workshop.NotPendingStatus');
          });
      });
    });
  });
});