module.exports = {
  getImportantConfigFileNames: () => [
    'db', 'jwt'
  ],

  getImportantConfigJSONSchema: () => [
    ['connection', 'db'],
    ['secret']
  ],

  tokenExpire: {
    jwt: '24h',
    refresh: () => {
      const date = new Date();
      date.setDate(date.getDate() + 60);
      return date;
    }
  }
};