module.exports = {
  front: {
    host: 'localhost',
    port: 80
  },

  backend: {
    host: 'localhost',
    port: 8080
  }
};