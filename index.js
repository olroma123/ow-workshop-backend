const Server = require('./app/server');
const Database = require('./app/lib/database');

const createTypeLogger = require('./app/lib/logger');
const appLogger = createTypeLogger('App');

if (!Server.checkForConfigFiles()) {
  throw new Error(`Не все конфиг-файлы существуют!`);
} else if (!Server.validateConfigFiles()) {
  throw new Error(`Какие-то поля в конфиг-файлах не существуют!`);
}

appLogger.info('Конфиги успешно проверены');

Database.connect().then(async () => {
  appLogger.info('Подключение к бд произошло успешно');

  Server.configureServer(8080, () => {
    appLogger.info('Сервер успешно запущен. Порт: 8080');
  });
}).catch(err => {
  appLogger.error(`Произошла ошибка подключения к базе данных: `, err);
});