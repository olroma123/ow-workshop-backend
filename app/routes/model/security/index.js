const router = require('express').Router();
const securityLogger = require('../../../lib/logger')('API-Security');

const expressValidation = require('express-validation');
const { loginSchema, registerSchema } = require('../../schema/security');
const tokenMiddleware = require('../../middleware/token');

const authRoute = require('./auth')(securityLogger);
const registerRoute = require('./register')(securityLogger);
const updateTokenRoute = require('./update-token')(securityLogger);

/**
 * @api { post } /security/register Регистрация пользователя
 * @apiName Register
 * @apiGroup Security
 *
 * @apiParam (Request body) { String } email E-mail пользователя
 * @apiParam (Request body) { String } password Пароль пользователя
 *
 * @apiSuccess { String } token JWT токен для запросов, которые требуют авторизацию
 * @apiSuccessExample { json } Success-response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": {
 *          "type": "success",
 *          "code": "API.OK"
 *       },
 *
 *       "payload": {
 *         "token": "JWT token"
 *       }
 *     }
 *
 * @apiError (API) WrongParams Не все параметры были переданы или формат данных неправильный. В <code>message.payload</code> указаны ошибки
 * @apiError (User) AlreadyExists Пользователь с таким E-mail уже существует
 * @apiError (Server) Error Серверная ошибка. Следует сообщить информацию администратору
 * @apiError (App) SyntaxError При парсинге параметров произошла синтаксическая ошибка
 * @apiErrorExample { json } Error-response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "message": {
 *         "type": "error",
 *         "code": "API.WrongParams",
 *         "payload": [
 *           [ "Поле email не может быть пустым" ]
 *         ]
 *       }
 *     }
 * */
router.post('/register', expressValidation(registerSchema), registerRoute);

/**
 * @api { post } /security/auth Авторизация пользователя
 * @apiName Auth
 * @apiGroup Security
 *
 * @apiParam (Request body) { String } email E-mail пользователя
 * @apiParam (Request body) { String } password Пароль пользователя
 *
 * @apiSuccess { String } token JWT токен для запросов, которые требуют авторизацию
 * @apiSuccessExample { json } Success-response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": {
 *          "type": "success",
 *          "code": "API.OK"
 *       },
 *
 *       "payload": {
 *         "token": "JWT token"
 *       }
 *     }
 *
 * @apiError (API) WrongParams Не все параметры были переданы или формат данных неправильный. В <code>message.payload</code> указаны ошибки
 * @apiError (Security) WrongLoginOrPassword Неправильный логин или пароль
 * @apiError (Server) Error Серверная ошибка. Следует сообщить информацию администратору
 * @apiError (App) SyntaxError При парсинге параметров произошла синтаксическая ошибка
 * @apiErrorExample { json } Error-response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "message": {
 *         "type": "error",
 *         "code": "API.WrongParams",
 *         "payload": [
 *           [ "Поле email не может быть пустым" ]
 *         ]
 *       }
 *     }
 * */
router.post('/auth', expressValidation(loginSchema), authRoute);

/**
 * @api { post } /security/update-token Обновление JWT токена с помощью refresh токена
 * @apiName UpdateToken
 * @apiGroup Security
 *
 * @apiHeader (Authorization) Bearer JWT token
 *
 * @apiSuccess { String } token новый JWT токен для запросов, которые требуют авторизацию
 * @apiSuccessExample { json } Success-response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": {
 *          "type": "success",
 *          "code": "API.OK"
 *       },
 *
 *       "payload": {
 *         "token": "JWT token"
 *       }
 *     }
 *
 *  @apiSuccessExample { json } Токен не нуждается в обновлении:
 *     HTTP/1.1 204 No Content
 *
 * @apiError (API) WrongParams Не все параметры были переданы или формат данных неправильный. В <code>message.payload</code> указаны ошибки
 * @apiError (Security) TokenRequired JWT токен не был передан
 * @apiError (Security) AccessDenied Refresh токена нет у пользователя в бд
 * @apiError (Server) Error Серверная ошибка. Следует сообщить информацию администратору
 * @apiError (App) SyntaxError При парсинге параметров произошла синтаксическая ошибка
 * @apiErrorExample { json } Error-response:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "message": {
 *         "type": "error",
 *         "code": "Security.TokenRequired"
 *       }
 *     }
 * */
router.post('/update-token', [ tokenMiddleware ], updateTokenRoute);

module.exports = router;