const apiMessages = require('../../../lib/api-messages');
const apiCode = require('../../../lib/api-codes');
const bcrypt = require('bcrypt');
const uuid = require('uuid/v4');

const UserModel = require('../../../models/user');

module.exports = logger => {
  return async (req, res) => {
    UserModel.getByEmail({ email: req.body.email })
      .then(user => {
        if (!user) {
          res.status(403).json(apiMessages.creator.createErrorMessage(apiCode.Security.WrongLoginOrPassword));
          throw null;
        }

        return Promise.all([user, bcrypt.compare(req.body.password, user.password)]);
      })
      // Проверка пароля
      .then(([user, isPasswordCorrect]) => {
        if (!isPasswordCorrect) {
          res.status(403).json(apiMessages.creator.createErrorMessage(apiCode.Security.WrongLoginOrPassword));
          throw null;
        }

        const refresh_token = uuid();
        return Promise.all([user, refresh_token, UserModel.addRefreshToken(user._id, refresh_token)]);
      })
      // Генерация JWT токена
      .then(([user, refresh_token, updateData]) => {
        res.json(apiMessages.creator.createSuccessMessage(apiCode.API.OK, {
          token: UserModel.generateJWTByUserObject(user, refresh_token)
        }));

        return UserModel.updateRefreshTokens(user._id);
      })
      .catch(err => {
        if (err === null) return;
        res.status(err.status || 500).json(err.error || apiMessages.types.server.serverError({
          error: err, logger
        }));
      });
  };
};