const apiMessages = require('../../../lib/api-messages');
const apiCode = require('../../../lib/api-codes');
const bcrypt = require('bcrypt');
const uuid = require('uuid/v4');

const UserModel = require('../../../models/user');

module.exports = logger => {
  return async (req, res) => {
    if (new Date() < new Date() + res.locals.userPayload.exp) {
      return res.status(204).send();
    }

    UserModel.getById({ id: res.locals.userPayload.id })
      .then(user => {
        const newRefreshToken = uuid();
        return Promise.all([
          user,
          newRefreshToken,
          UserModel.updateRefreshToken(user, res.locals.userPayload.refresh_token, newRefreshToken)
        ])
      })
      .then(([user, newRefreshToken, updateResult]) => {
        if (updateResult.error) {
          return res.status(403).json(apiMessages.creator.createErrorMessage(updateResult.code));
        }

        // Генерируем новый jwt и отправляем клиенту
        const jwt = UserModel.generateJWTByUserObject(user, newRefreshToken);
        res.json(apiMessages.creator.createSuccessMessage(apiCode.API.OK, jwt));
      })
      .catch(err => {
        res.status(err.status || 500).json(err.error || apiMessages.types.server.serverError({
          error: err, logger: logger
        }))
      })
  };
};