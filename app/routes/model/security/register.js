const apiMessages = require('../../../lib/api-messages');
const apiCode = require('../../../lib/api-codes');
const UserModel = require('../../../models/user');

module.exports = logger => {
  return async (req, res) => {
    UserModel.isExistsByEmail({ email: req.body.email })
      .then(exists => {
        if (exists) {
          res.status(409).json(apiMessages.creator.createErrorMessage(apiCode.User.AlreadyExists));
          return null;
        }

        return UserModel.add({email: req.body.email, password: req.body.password});
      })
      .then(userToken => {
        if (userToken) {
          res.json(apiMessages.creator.createSuccessMessage(apiCode.API.OK, {token: userToken}));
        }
      })
      .catch(err => {
        res.status(err.status || 500).json(err.error || apiMessages.types.server.serverError({
          error: err, logger: logger
        }));
      })
  };
};