const router = require('express').Router();

const workshopRouter = require('./workshop');
const securityRouter = require('./security');

router.use('/workshop', workshopRouter);
router.use('/security', securityRouter);

module.exports = router;