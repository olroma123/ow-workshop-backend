const apiMessage = require('../../../lib/api-messages');
const apiCodes = require('../../../lib/api-codes');
const axios = require('axios');
const WorkshopModel = require('../../../models/workshop');

module.exports = logger => {
  return async (req, res) => {
    // Проверка скриншотов
    const screensChecking = req.body.thumbnails.map(screen => axios.head(screen));
    try {
      const responses = await Promise.all(screensChecking);
      const wrongContentType = responses.find(response => {
        return !response.headers['content-type'] || !response.headers['content-type'].includes('image');
      });

      if (wrongContentType) {
        throw new Error();
      }
    } catch(err) {
      return res.status(400).json(apiMessage.creator.createErrorMessage(apiCodes.WorkShop.WrongThumbnails));
    }

    // Проверка на существование кода
    WorkshopModel.isExistsByCode({ code: req.body.code })
      .then(isExists => {
        if (isExists) {
          res.status(409).json(apiMessage.creator.createErrorMessage(apiCodes.WorkShop.CodeAlreadyExists));
          return null;
        }

        // Добавляем режим
        return WorkshopModel.add({
          title: req.body.title,
          smallDescription: req.body.smallDescription,
          thumbnails: req.body.thumbnails,
          code: req.body.code,
          userId: res.locals.userPayload.id
        });
      }).then(id => {
          if (id) {
            res.json(apiMessage.creator.createSuccessMessage(apiCodes.API.OK, { id: id }));
          }
      })
        .catch(err => {
          res.status(err.status || 500).json(apiMessage.types.server.serverError({
            error: err,
            logger: logger
          }));
        });
  };
};