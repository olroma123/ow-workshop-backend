const router = require('express').Router();
const workshopLogger = require('../../../lib/logger')('API-Workshop');

const expressValidation = require('express-validation');
const { addWorkshopItemSchema, endAddWorkshopItemSchema } = require('../../schema/workshop');
const tokenMiddleware = require('../../middleware/token');

const addNewItemRoute = require('./addNewItem')(workshopLogger);
const endAddRoute = require('./endAdd')(workshopLogger);

/**
 * @api { post } /workshop Добавление нового режима из мастерской. После успешного ответа необходимо вызвать POST /workshop/end
 * @apiName AddWorkshopItem
 * @apiGroup Workshop
 *
 * @apiParam (Request body) { String } title Название режима
 * @apiParam (Request body) { String } code Код мастерской
 * @apiParam (Request body) { String } smallDescription Краткое описание режима
 * @apiParam (Request body) { String[] } thumbnails Скриншоты из игры. Все скриншоты должны бать загружены на imgur
 *
 * @apiSuccess { String } id ID операции для отправки данных. См. POST /workshop/end
 * @apiSuccessExample { json } Success-response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": {
 *          "type": "success",
 *          "code": "OK"
 *       },
 *
 *       "payload": {
 *         "id": "ID"
 *       }
 *     }
 *
 * @apiError (User) NoAuthKey Пользователь не представился
 * @apiError (User) AccessDenied Неправильный JWT токен
 * @apiError (API) WrongParams Не все параметры были переданы или формат данных неправильный. В <code>message.payload</code> указаны ошибки
 * @apiError (Workshop) WrongThumbnails Какой-то скриншот не доступен публично или просто неправильный URL
 * @apiError (Workshop) CodeAlreadyExists Код режима уже добавлен на сайт
 * @apiError (Server) Error Серверная ошибка. Следует сообщить информацию администратору
 * @apiError (App) SyntaxError При парсинге параметров произошла синтаксическая ошибка
 * @apiErrorExample { json } Error-response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "message": {
 *         "type": "error",
 *         "code": "API.WrongParams",
 *         "payload": [
 *           [ "Поле title не может быть пустым" ]
 *         ]
 *       }
 *     }
 * */
router.post('/', [tokenMiddleware, expressValidation(addWorkshopItemSchema)], addNewItemRoute);

/**
 * @api { patch } /workshop/end Добавление полного описания для режима. Вызывать после успешного запроса на POST /workshop
 * @apiName EndAddWorkshopItem
 * @apiGroup Workshop
 *
 * @apiParam (Request body) { String } id ID добавленного режима
 * @apiParam (Request body) { String } full Description Полное описание режима
 *
 * @apiSuccessExample { json } Success-response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": {
 *          "type": "success",
 *          "code": "API.OK"
 *       },
 *
 *       "payload": null
 *     }
 *
 * @apiError (User) NoAuthKey Пользователь не представился
 * @apiError (User) AccessDenied Неправильный JWT токен
 * @apiError (API) NotValidId Неверный ID
 * @apiError (API) WrongParams Не все параметры были переданы или формат данных неправильный. В <code>message.payload</code> указаны ошибки
 * @apiError (Workshop) NotFound Переданный ID не найден в системе
 * @apiError (Workshop) NotHaveRightsToEdit Нет прав для вызова метода для этого пользователя
 * @apiError (Workshop) NotPendingStatus Данный api-метод уже вызывался для этого режима. Если вы хотите исправить описание, то см. PATCH /workshop/:id
 * @apiError (App) SyntaxError При парсинге параметров произошла синтаксическая ошибка
 * @apiErrorExample { json } Error-response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "message": {
 *         "type": "error",
 *         "code": "API.WrongParams",
 *         "payload": [
 *           [ "Поле id не может быть пустым" ]
 *         ]
 *       }
 *     }
 * */
router.patch('/end', [tokenMiddleware, expressValidation(endAddWorkshopItemSchema)], endAddRoute);

module.exports = router;