const apiMessage = require('../../../lib/api-messages');
const apiCode = require('../../../lib/api-codes');
const WorkshopModel = require('../../../models/workshop');

module.exports = logger => {
  return async (req, res) => {
    if (!WorkshopModel.isIdValid(req.body.id)) {
      return res.status(400).json(apiMessage.creator.createErrorMessage(apiCode.API.NotValidId));
    }

    // Проверяем статус режима. Должен быть pending
    WorkshopModel.getById({ id: req.body.id })
      .then(result => {
        if (!result) {
          res.status(400).json(apiMessage.creator.createErrorMessage(apiCode.WorkShop.NotFound));
          return null;
        }

        if (result.status !== 'pending') {
          res.status(400).json(apiMessage.creator.createErrorMessage(apiCode.WorkShop.NotPendingStatus));
          return null;
        }

        if (result.userId !== res.locals.userPayload.id) {
          res.status(403).json(apiMessage.creator.createErrorMessage(apiCode.WorkShop.NotHaveRightsToEdit));
          return null;
        }

        return WorkshopModel.endAdd(({id: req.body.id, fullDescription: req.body.fullDescription}));
      })
      .then(result => {
        if (result) {
          res.json(apiMessage.creator.createSuccessMessage(apiCode.API.OK));
        }
      })
      .catch(err => {
        res.status(err.status || 500).json(apiMessage.types.server.serverError({
          error: err, logger
        }));
      })
  };
};