const apiMessages = require('../../lib/api-messages');
const apiCode = require('../../lib/api-codes');

const jwtConfig = require('../../../config/jwt');
const jwt = require('jsonwebtoken');
const atob = require('atob');

module.exports = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    return res.status(401).json(apiMessages.creator.createErrorMessage(apiCode.Security.TokenRequired));
  }

  const partAuthHeader = authHeader.split(' ');
  if (partAuthHeader.length < 2 || partAuthHeader[0] !== 'Bearer') {
    return res.status(401).json(apiMessages.creator.createErrorMessage(apiCode.Security.TokenRequired));
  }

  jwt.verify(partAuthHeader[1], jwtConfig.secret, (err, decoded) => {
    if ((err && err.name !== 'TokenExpiredError') ||
       (err && err.name === 'TokenExpiredError' && !req.originalUrl.toString().includes('update-token'))) {
      return res.status(403).json(apiMessages.creator.createErrorMessage(apiCode.Security.AccessDenied));
    }

    res.locals.jwt = partAuthHeader[1];
    res.locals.userPayload = decoded ? decoded : JSON.parse(decodeURIComponent(escape(atob(partAuthHeader[1].split('.')[1]))));
    next();
  });
};