const apiMessages = require('../../lib/api-messages');
const apiCode = require('../../lib/api-codes');

module.exports = (roleName) => {
  return (req, res, next) => {
    if (typeof roleName !== typeof res.locals.userPayload.role.name || res.locals.userPayload.role.name !== roleName) {
      return res.status(403).json(apiMessages.creator.createErrorMessage(apiCode.Security.AccessDenied));
    }

    next();
  };
};