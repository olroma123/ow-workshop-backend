const defaultError = (error, field) => {
  switch(error.type) {
    case 'any.required': {
      return `Поле ${error.context.key} не заполнено`;
    }

    case 'any.empty': {
      return `Поле ${error.context.key} не может быть пустым`;
    }

    case 'string.base': {
      return `Поле ${error.context.key} должно иметь строковый тип`;
    }

    case 'string.regex.base': {
      return `Не соответсвует паттерну: ${error.context.pattern}`;
    }

    case 'string.email': {
      return `Неправильный E-mail`;
    }

    case 'array.base': {
      return `Поле ${error.context.key} должен быть массивом c правильными элементами`;
    }

    case 'array.min': {
      return `Поле ${error.context.key} должен иметь минимум ${error.context.limit} элемета`;
    }

    case 'array.includesOne': {
      if (error.context.reason) {
        error.context.reason.forEach((el, index) => {
          if (error.context.reason[index].type === 'any.empty') {
            error.context.reason[index].message = `Элемент в поле ${error.context.label} не может быть пустым`;
          } else {
            const contextError = defaultError(error.context.reason[index]);
            if (contextError) {
              error.context.reason[index].message = contextError;
            }
          }
        });

        return true;
      }
    }
  }
};

module.exports = defaultError;