const Joi = require('joi');
const errorReport = require('../error');
const schemaLogger = require('../../../lib/logger')('Schema.AddWorkshopItem');

module.exports = {
  body: {
    title: Joi.string().required().error(errors => {
      errors.forEach(err => {
        const defaultError = errorReport(err);
        if (defaultError) {
          err.message = defaultError;
        } else {
          schemaLogger.warn(`Неопознанный тип ошибки: ${err.type}`);
        }
      });

      return errors;
    }),

    code: Joi.string().required().error(errors => {
      errors.forEach(err => {
        const defaultError = errorReport(err);
        if (defaultError) {
          err.message = defaultError;
        } else {
          schemaLogger.warn(`Неопознанный тип ошибки: ${err.type}`);
        }
      });

      return errors;
    }),

    smallDescription: Joi.string().required().error(errors => {
      errors.forEach(err => {
        const defaultError = errorReport(err);
        if (defaultError) {
          err.message = defaultError;
        } else {
          schemaLogger.warn(`Неопознанный тип ошибки: ${err.type}`);
        }
      });
      return errors;
    }),

    thumbnails: Joi.array().min(3).required().items(Joi.string().regex(/https:\/\/i\.imgur\.com\//)).error(errors => {
      errors.forEach(err => {
        const defaultError = errorReport(err);
        if (defaultError) {
          err.message = defaultError;
        } else {
          schemaLogger.warn(`Неопознанный тип ошибки: ${err.type}`, err);
        }
      });
      return errors;
    })
  }
};