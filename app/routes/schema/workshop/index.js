const addWorkshopItemSchema = require('./add');
const endAddWorkshopItemSchema = require('./end');

module.exports = {
  addWorkshopItemSchema,
  endAddWorkshopItemSchema
};