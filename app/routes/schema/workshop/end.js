const Joi = require('joi');
const errorReport = require('../error');
const schemaLogger = require('../../../lib/logger')('Schema.AddWorkshopItem');

module.exports = {
  body: {
    id: Joi.string().required().error(errors => {
      errors.forEach(err => {
        const defaultError = errorReport(err);
        if (defaultError) {
          err.message = defaultError;
        } else {
          schemaLogger.warn(`Неопознанный тип ошибки: ${err.type}`);
        }
      });
      return errors;
    }),

    fullDescription: Joi.string().required().error(errors => {
      errors.forEach(err => {
        const defaultError = errorReport(err);
        if (defaultError) {
          err.message = defaultError;
        } else {
          schemaLogger.warn(`Неопознанный тип ошибки: ${err.type}`);
        }
      });
      return errors;
    })
  }
};