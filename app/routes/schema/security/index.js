const loginSchema = require('./login');
const registerSchema = require('./register');

module.exports = {
  loginSchema,
  registerSchema
};