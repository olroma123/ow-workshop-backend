const Joi = require('joi');
const errorReport = require('../error');
const schemaLogger = require('../../../lib/logger')('Schema.Register');

module.exports = {
  body: {
    email: Joi.string().required().email().error(errors => {
      errors.forEach(err => {
        const defaultError = errorReport(err);
        if (defaultError) {
          err.message = defaultError;
        } else {
          schemaLogger.warn(`Неопознанный тип ошибки: ${err.type}`);
        }
      });

      return errors;
    }),

    password: Joi.string().required().error(errors => {
      errors.forEach(err => {
        const defaultError = errorReport(err);
        if (defaultError) {
          err.message = defaultError;
        } else {
          schemaLogger.warn(`Неопознанный тип ошибки: ${err.type}`);
        }
      });

      return errors;
    })
  }
};
