const winston = require('winston');

const messageTemplate = winston.format.printf(({level, message, label, timestamp}) => {
  return `${timestamp} [${label}] ${level} - ${message}`;
});

/**
 * Создает "тип" логирования.
 * @example
 *    createLoggerType("API");  // [API] - <Сообщение>
 * @return { winston.Logger } Возвращает Логгер
 * */
const createLoggerType = (loggerName) => {
  return winston.createLogger({
    format: winston.format.combine(
      winston.format.label({label: loggerName}),
      winston.format.timestamp(),
      winston.format.colorize(),
      messageTemplate
    ),
    level: 'debug',
    transports: [
      new winston.transports.Console(),
    ]
  });
};

module.exports = createLoggerType;