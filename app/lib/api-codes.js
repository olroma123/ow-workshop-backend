module.exports = {
  App: {
    SyntaxError: 'App.SyntaxError'
  },

  Server: {
    Error: 'Server.Error'
  },

  API: {
    WrongParams: 'API.WrongParams',
    OK: 'API.OK',
    NotValidId: 'API.NotValidId'
  },

  Security: {
    AccessDenied: 'Security.AccessDenied',
    TokenRequired: 'Security.TokenRequired',
    WrongLoginOrPassword: 'Security.WrongLoginOrPassword'
  },

  User: {
    AlreadyExists: 'User.AlreadyExists',
    NotFound: 'User.NotFound',
    NoAuthKey: 'User.NoAuthKey'
  },

  WorkShop: {
    WrongThumbnails: 'Workshop.WrongThumbnails',
    CodeAlreadyExists: 'Workshop.CodeAlreadyExists',
    NotFound: 'Workshop.NotFound',
    NotHaveRightsToEdit: 'Workshop.NotHaveRightsToEdit',
    NotPendingStatus: 'Workshop.NotPendingStatus'
  }
};