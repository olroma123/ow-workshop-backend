const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const dbConfig = require('../../config/db');

let database = null;
const objectIdRegex = new RegExp("^[0-9a-fA-F]{24}$");

module.exports = {
  /**
   * Метод для коннекта к базе данных
   * @return { Promise<null> } Возвращает промис, resolve - успешное подключение
   * */
  connect: () => {
    return new Promise((resolve, reject) => {
      MongoClient.connect(dbConfig.connection, { auth: dbConfig.auth, useNewUrlParser: true })
        .then(db => {
          database = db.db(dbConfig.db);
          resolve();
        })
        .catch(err => reject(err));
    });

  },

  /**
   * Метод для получения коллекции по имени
   * @param { string } collectionName Коллеция
   * @return { Collection } Возвращает объект Mongo (Db) для CRUD операций
   * */
  collection: (collectionName) => {
    if (!database) {
      throw new Error(`База данных не подключена!`);
    }

    return database.collection(collectionName);
  },

  /**
   * Метод для получения состояния подключения к базе данных
   * @return { boolean } Возвращает true если подключение активно
   * */
  isConnect: () => database !== null,

  /**
   * Получение ObjectId
   * @param { string } id Текстовое представление ID
   * @return { ObjectId } Возвращает объект типа ObjectId для использования с mongo
   * */
  ObjectId: (id) => mongo.ObjectID(id),

  /**
   * Проверка ID на валидность
   * @param { string } id Текстовое представление ID
   * @return { boolean } Вернет true если ID валиден
   * */
  isObjectIdValid: (id) => objectIdRegex.test(id)
};