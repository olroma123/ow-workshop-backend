const creatorMessage = require('../creator');

module.exports = {
  roleNotFound: () => {
    return creatorMessage.createErrorMessage('Роль не найдена');
  }
};