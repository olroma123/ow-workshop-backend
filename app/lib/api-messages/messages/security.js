const creator = require('../creator');

module.exports = {
  accessDenied: () => {
    return creator.createErrorMessage('Security.AccessDenied')
  },

  authorizationNeed: () => {
    return creator.createErrorMessage('Security.TokenRequired')
  },

  wrongLoginOrPassword: () => {
    return creator.createErrorMessage('Security.WrongLoginOrPassword')
  }
};