const creator = require('../creator');

module.exports = {
  serverError: ({error = null, logger} = null) => {
    if (process.env.NODE_ENV !== 'production' && error !== null) {
      logger.error(error);
    }

    return creator.createErrorMessage('Server.Error');
  },
};