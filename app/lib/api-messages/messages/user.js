const creatorMessage = require('../creator');

module.exports = {
  userAlreadyExists: () => {
    return creatorMessage.createErrorMessage('User.AlreadyExists');
  },

  userNotFound: () => {
    return creatorMessage.createErrorMessage('User.NotFound');
  }
};