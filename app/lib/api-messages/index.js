const securityMessages = require('./messages/security');
const serverMessages = require('./messages/server');
const userMessages = require('./messages/user');
const roleMessages = require('./messages/roles');

const creatorMessages = require('./creator');

module.exports = {
  types: {
    security: securityMessages,
    server: serverMessages,
    user: userMessages,
    role: roleMessages,
  },

  creator: creatorMessages
};