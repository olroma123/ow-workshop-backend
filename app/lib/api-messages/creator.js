const createMessage = (type, code, messagePayload = undefined, payload = null) => {
  return {
    message: {
      type, code,
      payload: messagePayload
    },

    payload
  };
};

module.exports = {
  createMessage,

  /**
   * Создание сообщения с типом "success"
   * @param { string | array } code Текст сообщения
   * @param { object | array } payload Полезная информация
   * @return { Object } Возвращает объект с сообщением: { message: {type, text}, payload }
   * */
  createSuccessMessage: (code, payload = null) => {
    return createMessage('success', code, undefined, payload);
  },

  /**
   * Создание сообщения с типом "error"
   * @param { string | array } code Текст сообщения
   * @param { object, array } messagePayload Полезная информация для сообщений
   * @param { object, array } payload Полезная информация
   * @return { Object } Возвращает объект с сообщением: { message: {type, text}, payload }
   * */
  createErrorMessage: (code, messagePayload = undefined, payload = null) => {
    return createMessage('error', code, messagePayload, payload);
  }
};