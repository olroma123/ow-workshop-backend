const database = require('../lib/database');
const apiCode = require('../lib/api-codes');
const bcrypt = require('bcrypt');
const uuid = require('uuid/v4');
const jwt = require('jsonwebtoken');
const jsonSecret = require('../../config/jwt');
const config = require('../../config');

const RoleModel = require('../models/role');

class UserModel {
  /**
   * Получение данных о пользователе по ID
   * @param { string } id ID пользователя
   * */
  static getById = ({ id }) => database.collection('users').findOne({ _id: database.ObjectId(id) });

  /**
   * Получение данных о пользователе по Email
   * @param { string } email E-mail пользователя
   * */
  static getByEmail = ({ email }) => database.collection('users').findOne({ email });

  /**
   * Метод для проверки существования пользователя по E-mail
   * @param { string } email E-mail пользователя
   * @return { Promise<boolean> } Вернет true если пользователь существует
   * */
  static isExistsByEmail({ email }) {
    return new Promise((resolve, reject) => {
      UserModel.getByEmail({ email })
        .then(result => resolve(!!result))
        .catch(err => reject(err));
    });
  }

  /**
   * Метод для проверки существования пользователя по ID
   * @param { string } id ID пользователя
   * @return { Promise<boolean> } Вернет true если пользователь существует
   * */
  static isExistsById({ id }) {
    return new Promise((resolve, reject) => {
      UserModel.getById({ id })
        .then(result => resolve(!!result))
        .catch(err => reject(err));
    });
  }

  /**
   * Метод для добавляения нового пользователя в базу данных
   * @param { string } email E-mail пользователя
   * @param { string } password Пароль пользователя
   * @return { string } Возвращает JWT токен с информацией о зарегистрированном пользователе
   * */
  static async add({email, password}) {
    const passwordHash = await bcrypt.hash(password, 6);
    const userRole = RoleModel.getUserRole();

    const userObject = {
      email,
      password: passwordHash,
      role: userRole,
      refresh_tokens: [{
        token: uuid(),
        expiresIn: config.tokenExpire.refresh()
      }]
    };

    try {
      const result = await database.collection('users').insertOne(userObject);
      return UserModel.generateJWTByUserObject({
        _id: result.ops[0]._id,
        email: userObject.email,
        role: userRole,
      }, userObject.refresh_tokens[0].token);
    } catch (e) {
      throw new Error(e);
    }
  }

  /**
   * Метод для изменение роли пользователя
   * @param { string } id ID пользователя
   * @param { object } role Объект с полной информацией о роли
   * */
  static changeRole(id, role) {
    return new Promise((resolve, reject) => {
      database.collection('users').updateOne({_id: database.ObjectId(id)}, {
        $set: { role }
      }).then(() => resolve())
        .catch(err => reject(err));
    })
  }

  /**
   * Метод для удаления пользователя по id
   * @param { string } id Id пользователя
   * @return { Promise }
   * */
  static deleteById(id) {
    return new Promise((resolve, reject) => {
      database.collection('users').deleteOne({_id: database.ObjectId(id)})
        .then(data => {
          if (data.deletedCount === 0) {
            reject('Пользователь не найден');
          }

          resolve();
        });
    });
  }

  /**
   * Метод для удаления пользователя по e-mail
   * @param { string } email E-mail пользователя
   * @return { Promise }
   * */
  static deleteByEmail({ email }) {
    return new Promise((resolve, reject) => {
      database.collection('users').deleteOne({ email })
        .then(data => {
          if (data.deletedCount === 0) {
            reject('Пользователь не найден');
          }

          resolve();
        });
    });
  }

  /**
   * Генерация JWT токена из объекта с пользовательскими данными
   * @param { object } user Объект с данными о пользователе
   * @param { string } user._id ID пользователя
   * @param { string } user.email E-mail пользователя
   * @param { string } user.role Роль пользователя
   * @param { string | null } refresh_token Токен для обновления JWT. Если передан null - генерирует рандомный
   * @return { string } Возвращает сгенерированный токен
   * */
  static generateJWTByUserObject(user, refresh_token = null) {
    return jwt.sign({
      email: user.email,
      id: user._id,
      role: {
        title: user.role.title,
        name: user.role.name
      },
      refresh_token: refresh_token || uuid(),
    }, jsonSecret.secret, { expiresIn: config.tokenExpire.jwt });
  }

  /**
   * Добавление нового refresh токена в массив
   * @param { string } id ID пользователя
   * @param { string } refresh_token Новый refresh токен
   * @return { Promise } Возвращает Promise от updateOne функции
   * */
  static addRefreshToken(id, refresh_token) {
    return database.collection('users').updateOne({_id: database.ObjectId(id)}, {
      $push: {refresh_tokens: {
          token: refresh_token,
          expiresIn: config.tokenExpire.refresh()
        }}
    });
  }

  /**
   * Обновляет список refresh токенов у пользователя, проверяя время
   * @param { string } id ID пользователя
   * */
  static updateRefreshTokens(id) {
    return new Promise((resolve, reject) => {
      UserModel.getById({ id })
        .then(user => {
          let refresh_tokens = user.refresh_tokens.filter(token => {
            return new Date() < new Date(token.expiresIn);
          });

          return database.collection('users').updateOne({_id: id}, {
            $set: { refresh_tokens }
          })
        })
        .then(() => resolve())
        .catch(err => reject(err));
    })
  }

  /**
   * Заменяет refresh токен другим токеном
   * @param { object } user Объект с информацией о пользователей
   * @param { string } old_refresh_token Старый токен, который нужно заменить
   * @param { string } new_refresh_token Новый токен
   * */
  static updateRefreshToken(user, old_refresh_token, new_refresh_token) {
    return new Promise((resolve, reject) => {
      let refresh_tokens = user.refresh_tokens.slice();
      const oldTokenIndex = refresh_tokens.findIndex(item => item.token === old_refresh_token);

      // Если токен не был найден
      // То значит он был удален
      if (oldTokenIndex === -1) {
        resolve({
          error: true,
          code: apiCode.Security.AccessDenied
        });
      }

      // Удаляем прошлый refresh и добавляем новый сгенерированный
      refresh_tokens.splice(oldTokenIndex, 1);
      refresh_tokens.push({
        token: new_refresh_token,
        expiresIn: config.tokenExpire.refresh()
      });

      database.collection('users').updateOne({_id: database.ObjectId(user._id)}, {
        $set: { refresh_tokens }
      }).then(() => resolve())
        .catch(err => reject(err));
    });
  }
}

module.exports = UserModel;