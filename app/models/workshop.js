const apiMessages = require('../lib/api-messages');
const database = require('../lib/database');

class WorkshopModel  {
  /**
   * Добавление режима в бд
   * @param { string } title Название режима
   * @param { string } smallDescription Краткое описание режима
   * @param { string } code Код режима
   * @param { string } userId ID пользователя, который добавил режим
   * @param { Array<string> } thumbnails Скриншоты режима, загруженные на i.imgur
   * @return { Promise<string> } Возвращает ID для следующей операции
   * */
  static add({title, thumbnails, smallDescription, code, userId}) {
    return new Promise((resolve, reject) => {
      database.collection('workshop').insertOne({
        title, thumbnails, smallDescription, code, userId,
        status: 'pending'
      }).then(result => resolve(result.insertedId.toString()))
        .catch(err => reject(err));
    });
  };

  /**
   * Метод для добавления полного описания к режиму
   * @param { string } id ID режима в бд
   * @param { string } fullDescription Полное описание режима
   * */
  static endAdd({id, fullDescription}) {
    return database.collection('workshop').updateOne({_id: database.ObjectId(id)}, {
      $set: { fullDescription, status: 'added' }
    });
  }

  static updateChangelog({ text }) {

  };

  /**
   * Метод для проверки существования режима по ID
   * @param { string } id ID режима в бд
   * @return { Promise<boolean> } Вернет true если ID найден
   * */
  static isExistsById({ id }) {
    return new Promise((resolve, reject) => {
      WorkshopModel.getById({ id })
        .then(result => resolve(!!result))
        .catch(err => reject(err));
    });
  }

  /**
   * Метод для проверки существования режима по коду
   * @param { string } code Код режима
   * @return { Promise<boolean> } Вернет true если код найден
   * */
  static isExistsByCode({ code }) {
    return new Promise((resolve, reject) => {
      WorkshopModel.getByCode({ code })
        .then(result => resolve(!!result))
        .catch(err => reject(err));
    });
  }

  /**
   * Получение полной инфорации о режиме по коду
   * @param { string } code Код режима
   * */
  static getByCode = ({ code }) => database.collection('workshop').findOne({ code });

  /**
   * Получение полной инфорации о режиме по ID
   * @param { string } id ID режима в бд
   * */
  static getById({ id }) {
    return new Promise((resolve, reject) => {
      database.collection('workshop').findOne({_id: database.ObjectId(id)})
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  }

  static isIdValid = database.isObjectIdValid;

  static all() {

  }
}

module.exports = WorkshopModel;