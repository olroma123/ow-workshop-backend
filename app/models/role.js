/**
 *  @typedef RoleInfo
 *  @type { object }
 *  @property { string } title Название роли для вывода на фронт
 *  @property { string } name Название роли для тех. пользования
 * */

class RoleModel {
  /**
   * Метод для получения данных о роли: пользователь
   * @return { RoleInfo } Возвращает полную информацию о роли { name, title }
   * */
  static getUserRole() {
    return {
      name: 'user',
      title: 'Пользователь'
    };
  }

  /**
   * Метод для получения данных о роли: пользователь
   * @return { RoleInfo } Возвращает полную информацию о роли { name, title }
   * */
  static getAdminRole() {
    return {
      name: 'admin',
      title: 'Администратор'
    };
  }
}

module.exports = RoleModel;