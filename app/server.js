const express = require('express');
const expressValidation = require('express-validation');
const config = require('../config');
const fs = require('fs');
const loggerCreator = require('./lib/logger');
const serverLogger = loggerCreator('Server');

const routes = require('./routes/model');
const creatorMessages = require('./lib/api-messages/creator');
const apiCodes = require('./lib/api-codes');

class Server {
  /**
   * Метод для простой проверки файлов-конфигов на наличие их в /config папке
   * @return { boolean } Возвращает true если нужные файлы существуют
   * */
  static checkForConfigFiles() {
    config.getImportantConfigFileNames().forEach(configName => {
      if (!fs.existsSync(`config/${configName}.json`)) {
        return false;
      }
    });

    return true;
  }

  /**
   * Метод для проверки конфигов на валидность
   * @return { boolean } Возвращает true если все конфиг-файлы прошли проверки
   * */
  static validateConfigFiles() {
    const importantConfigs = config.getImportantConfigFileNames();
    const importantConfigsSchema = config.getImportantConfigJSONSchema();

    importantConfigs.forEach((configName, index) => {
      const currentConfig = JSON.parse(fs.readFileSync(`config/${configName}.json`, 'utf-8'));
      const keys = Object.keys(currentConfig);

      importantConfigsSchema[index].forEach(configSchemaField => {
        if (!keys.includes(configSchemaField)) {
          return false;
        }
      });
    });

    return true;
  }

  /**
   * Создает экземпляр express'а и конфигурирует его
   * @return { express } Возвращает объект типа Express
   * */
  static configureServer(port, startSuccessCb) {
    return express()
      .use(express.json())
      .use(routes)
      .use('/api-docs', express.static('./apidoc'))
      .use((err, req, res, next) => {
        if (err instanceof expressValidation.ValidationError) {
          return res.status(err.status).json(creatorMessages.createErrorMessage(
            apiCodes.API.WrongParams,
            [...new Set(err.errors.map(error => error.messages))]
          ));
        }

        else {
          switch(err.type) {
            case 'entity.parse.failed': {
              res.status(400).json(creatorMessages.createErrorMessage(
                apiCodes.App.SyntaxError
              ));
              break;
            }

            default: {
              serverLogger.error(err);
              break;
            }
          }
        }
      })
      .listen(port, startSuccessCb);
  }
}

module.exports = Server;