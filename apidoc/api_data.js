define({ "api": [
  {
    "type": " post ",
    "url": "/security/auth",
    "title": "Авторизация пользователя",
    "name": "Auth",
    "group": "Security",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>E-mail пользователя</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Пароль пользователя</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT токен для запросов, которые требуют авторизацию</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": {\n     \"type\": \"success\",\n     \"code\": \"API.OK\"\n  },\n\n  \"payload\": {\n    \"token\": \"JWT token\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "API": [
          {
            "group": "API",
            "optional": false,
            "field": "WrongParams",
            "description": "<p>Не все параметры были переданы или формат данных неправильный. В <code>message.payload</code> указаны ошибки</p>"
          }
        ],
        "Security": [
          {
            "group": "Security",
            "optional": false,
            "field": "WrongLoginOrPassword",
            "description": "<p>Неправильный логин или пароль</p>"
          }
        ],
        "Server": [
          {
            "group": "Server",
            "optional": false,
            "field": "Error",
            "description": "<p>Серверная ошибка. Следует сообщить информацию администратору</p>"
          }
        ],
        "App": [
          {
            "group": "App",
            "optional": false,
            "field": "SyntaxError",
            "description": "<p>При парсинге параметров произошла синтаксическая ошибка</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"message\": {\n    \"type\": \"error\",\n    \"code\": \"API.WrongParams\",\n    \"payload\": [\n      [ \"Поле email не может быть пустым\" ]\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/routes/model/security/index.js",
    "groupTitle": "Security"
  },
  {
    "type": " post ",
    "url": "/security/register",
    "title": "Регистрация пользователя",
    "name": "Register",
    "group": "Security",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>E-mail пользователя</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Пароль пользователя</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT токен для запросов, которые требуют авторизацию</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": {\n     \"type\": \"success\",\n     \"code\": \"API.OK\"\n  },\n\n  \"payload\": {\n    \"token\": \"JWT token\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "API": [
          {
            "group": "API",
            "optional": false,
            "field": "WrongParams",
            "description": "<p>Не все параметры были переданы или формат данных неправильный. В <code>message.payload</code> указаны ошибки</p>"
          }
        ],
        "User": [
          {
            "group": "User",
            "optional": false,
            "field": "AlreadyExists",
            "description": "<p>Пользователь с таким E-mail уже существует</p>"
          }
        ],
        "Server": [
          {
            "group": "Server",
            "optional": false,
            "field": "Error",
            "description": "<p>Серверная ошибка. Следует сообщить информацию администратору</p>"
          }
        ],
        "App": [
          {
            "group": "App",
            "optional": false,
            "field": "SyntaxError",
            "description": "<p>При парсинге параметров произошла синтаксическая ошибка</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"message\": {\n    \"type\": \"error\",\n    \"code\": \"API.WrongParams\",\n    \"payload\": [\n      [ \"Поле email не может быть пустым\" ]\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/routes/model/security/index.js",
    "groupTitle": "Security"
  },
  {
    "type": " post ",
    "url": "/security/update-token",
    "title": "Обновление JWT токена с помощью refresh токена",
    "name": "UpdateToken",
    "group": "Security",
    "header": {
      "fields": {
        "Authorization": [
          {
            "group": "Authorization",
            "optional": false,
            "field": "Bearer",
            "description": "<p>JWT token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>новый JWT токен для запросов, которые требуют авторизацию</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": {\n     \"type\": \"success\",\n     \"code\": \"API.OK\"\n  },\n\n  \"payload\": {\n    \"token\": \"JWT token\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Токен не нуждается в обновлении:",
          "content": "HTTP/1.1 204 No Content",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "API": [
          {
            "group": "API",
            "optional": false,
            "field": "WrongParams",
            "description": "<p>Не все параметры были переданы или формат данных неправильный. В <code>message.payload</code> указаны ошибки</p>"
          }
        ],
        "Security": [
          {
            "group": "Security",
            "optional": false,
            "field": "TokenRequired",
            "description": "<p>JWT токен не был передан</p>"
          },
          {
            "group": "Security",
            "optional": false,
            "field": "AccessDenied",
            "description": "<p>Refresh токена нет у пользователя в бд</p>"
          }
        ],
        "Server": [
          {
            "group": "Server",
            "optional": false,
            "field": "Error",
            "description": "<p>Серверная ошибка. Следует сообщить информацию администратору</p>"
          }
        ],
        "App": [
          {
            "group": "App",
            "optional": false,
            "field": "SyntaxError",
            "description": "<p>При парсинге параметров произошла синтаксическая ошибка</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-response:",
          "content": "HTTP/1.1 401 Bad Request\n{\n  \"message\": {\n    \"type\": \"error\",\n    \"code\": \"Security.TokenRequired\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/routes/model/security/index.js",
    "groupTitle": "Security"
  },
  {
    "type": " post ",
    "url": "/workshop",
    "title": "Добавление нового режима из мастерской. После успешного ответа необходимо вызвать POST /workshop/end",
    "name": "AddWorkshopItem",
    "group": "Workshop",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Название режима</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>Код мастерской</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "smallDescription",
            "description": "<p>Краткое описание режима</p>"
          },
          {
            "group": "Request body",
            "type": "String[]",
            "optional": false,
            "field": "thumbnails",
            "description": "<p>Скриншоты из игры. Все скриншоты должны бать загружены на imgur</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID операции для отправки данных. См. POST /workshop/end</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": {\n     \"type\": \"success\",\n     \"code\": \"OK\"\n  },\n\n  \"payload\": {\n    \"id\": \"ID\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "User": [
          {
            "group": "User",
            "optional": false,
            "field": "NoAuthKey",
            "description": "<p>Пользователь не представился</p>"
          },
          {
            "group": "User",
            "optional": false,
            "field": "AccessDenied",
            "description": "<p>Неправильный JWT токен</p>"
          }
        ],
        "API": [
          {
            "group": "API",
            "optional": false,
            "field": "WrongParams",
            "description": "<p>Не все параметры были переданы или формат данных неправильный. В <code>message.payload</code> указаны ошибки</p>"
          }
        ],
        "Workshop": [
          {
            "group": "Workshop",
            "optional": false,
            "field": "WrongThumbnails",
            "description": "<p>Какой-то скриншот не доступен публично или просто неправильный URL</p>"
          },
          {
            "group": "Workshop",
            "optional": false,
            "field": "CodeAlreadyExists",
            "description": "<p>Код режима уже добавлен на сайт</p>"
          }
        ],
        "Server": [
          {
            "group": "Server",
            "optional": false,
            "field": "Error",
            "description": "<p>Серверная ошибка. Следует сообщить информацию администратору</p>"
          }
        ],
        "App": [
          {
            "group": "App",
            "optional": false,
            "field": "SyntaxError",
            "description": "<p>При парсинге параметров произошла синтаксическая ошибка</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"message\": {\n    \"type\": \"error\",\n    \"code\": \"API.WrongParams\",\n    \"payload\": [\n      [ \"Поле title не может быть пустым\" ]\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/routes/model/workshop/index.js",
    "groupTitle": "Workshop"
  },
  {
    "type": " patch ",
    "url": "/workshop/end",
    "title": "Добавление полного описания для режима. Вызывать после успешного запроса на POST /workshop",
    "name": "EndAddWorkshopItem",
    "group": "Workshop",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID добавленного режима</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "full",
            "description": "<p>Description Полное описание режима</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": {\n     \"type\": \"success\",\n     \"code\": \"API.OK\"\n  },\n\n  \"payload\": null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "User": [
          {
            "group": "User",
            "optional": false,
            "field": "NoAuthKey",
            "description": "<p>Пользователь не представился</p>"
          },
          {
            "group": "User",
            "optional": false,
            "field": "AccessDenied",
            "description": "<p>Неправильный JWT токен</p>"
          }
        ],
        "API": [
          {
            "group": "API",
            "optional": false,
            "field": "NotValidId",
            "description": "<p>Неверный ID</p>"
          },
          {
            "group": "API",
            "optional": false,
            "field": "WrongParams",
            "description": "<p>Не все параметры были переданы или формат данных неправильный. В <code>message.payload</code> указаны ошибки</p>"
          }
        ],
        "Workshop": [
          {
            "group": "Workshop",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Переданный ID не найден в системе</p>"
          },
          {
            "group": "Workshop",
            "optional": false,
            "field": "NotPendingStatus",
            "description": "<p>Данный api-метод уже вызывался для этого режима. Если вы хотите исправить описание, то см. PATCH /workshop/:id</p>"
          }
        ],
        "App": [
          {
            "group": "App",
            "optional": false,
            "field": "SyntaxError",
            "description": "<p>При парсинге параметров произошла синтаксическая ошибка</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"message\": {\n    \"type\": \"error\",\n    \"code\": \"API.WrongParams\",\n    \"payload\": [\n      [ \"Поле id не может быть пустым\" ]\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/routes/model/workshop/index.js",
    "groupTitle": "Workshop"
  }
] });
