module.exports = {
  apps : [{
    name: 'OW-API',
    script: 'index.js',
    instances: 1,
    autorestart: true,
    watch: true,
    ignore_watch: [
      'node_modules'
    ],
    watch_delay: 500,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
};
